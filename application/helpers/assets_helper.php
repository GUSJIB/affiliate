<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

   function asset_path($path = null) {
      return base_url().$path;
   }

   function image_path($path = null) {
      return base_url().'assets/images/'.$path;
   }

?>