<?php

class MY_FrontEndController extends CI_Controller {

   function __construct()
   {
      parent::__construct();
      $this->load->library('Aauth');
      if($this->aauth->is_loggedin()){
         if($this->aauth->is_admin()){
            redirect('/dashboard');
         }else{
            redirect('/profiles');
         }
      }
      
      $this->_init();
   }

   private function _init() {
      // set template
      $this->output->set_template('frontend');
      
      // Load javascript
      $js = array(
         "assets/components/jquery/dist/jquery.min.js",
         "assets/components/bootstrap/dist/js/bootstrap.min.js",
         "assets/components/fastclick/lib/fastclick.js",
         "assets/components/nprogress/nprogress.js",
         "assets/components/jQuery-Smart-Wizard/js/jquery.smartWizard.js",
         "assets/components/moment/moment.js",
         "assets/themes/Frontend/js/datepicker/daterangepicker.js",
         "assets/components/select2/dist/js/select2.full.min.js",
         "assets/components/devbridge-autocomplete/dist/jquery.autocomplete.min.js",
         "assets/components/validator/validator.js",
         "assets/themes/Frontend/js/custom.js",
         "assets/themes/Frontend/js/script.js",
      );
      foreach ($js as $value) {
         $this->load->js($value);  
      }

      // Load StyleSheet
      $css = array(
         "assets/components/bootstrap/dist/css/bootstrap.min.css",
         "assets/components/animate.css/animate.min.css",
         "assets/components/font-awesome/css/font-awesome.min.css",
         "assets/components/select2/dist/css/select2.min.css",
         "assets/themes/Frontend/css/custom.css",
         "assets/themes/Frontend/css/main.css"
      );
      foreach ($css as $value) {
         $this->load->css($value);  
      }
   }

}

class MY_BackEndController extends CI_Controller {

   function __construct()
   {
      parent::__construct();
      $this->load->library('Aauth');
      $this->load->model('Menu');
      if(!$this->aauth->is_loggedin()){
         redirect('/signin');
      }
      $this->_init();
      $this->_section();
   }

   private function _init() {
      // set template
      $this->output->set_template('admin');
      
      // Load javascript
      $js = array(
         "assets/components/jquery/dist/jquery.min.js",
         "assets/components/bootstrap/dist/js/bootstrap.min.js",
         "assets/components/fastclick/lib/fastclick.js",
         "assets/components/nprogress/nprogress.js",
         "assets/components/jQuery-Smart-Wizard/js/jquery.smartWizard.js"
      );
      foreach ($js as $value) {
         $this->load->js($value);  
      }

      // Load StyleSheet
      $css = array(
         "assets/components/bootstrap/dist/css/bootstrap.min.css",
         "assets/components/font-awesome/css/font-awesome.min.css",
         "assets/themes/Frontend/css/custom.min.css",
         "assets/themes/Frontend/css/main.css"
      );
      foreach ($css as $value) {
         $this->load->css($value);  
      }
   }

   private function _section() {
      $this->load->section('sidebar', 'admin/sidebar_menu', $this->load_user_data());
      $this->load->section('topright', 'admin/topright_menu', $this->load_user_data());
      // $this->load->section('messages', 'admin/personal_message', $this->load_user_message());
   }

   private function load_user_data(){
      $this->load->model('Profile');
      $user = $this->aauth->get_user();
      return $this->Profile->where('user_id', $user->id)->get();
   }

   private function load_user_message(){

   }

}