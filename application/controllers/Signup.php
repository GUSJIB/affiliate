<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signup extends MY_FrontEndController {

   function __construct() {
      parent::__construct();
      $this->load->library("Aauth");
      $this->load->helper('assets_helper');
      $this->load->model('Profile');
   }

   public function index() {
      $this->load->js("assets/themes/Frontend/js/signup.js");
      $this->output->set_title('Affiliate Program - Signup');

      $data['errors'] = array();

      if($this->input->post()){
         $email = $this->input->post('email');
         $pass = $this->input->post('pass');
         
         if($this->aauth->create_user($email, $pass)){
            $user_id = $this->aauth->get_user_id($email);
            $this->Profile->insert(array_merge(array('user_id' => $user_id), $this->input->post('profile')));

            $profile = $this->input->post('profile');

            // Imprementation Send Email update Coupon code
            $this->load->library('email');
            $this->email->initialize(array(
              'protocol' => 'smtp',
              'smtp_host' => 'smtp.sendgrid.net',
              'smtp_user' => 'brushwork8',
              'smtp_pass' => 'cosmetics1234',
              'smtp_port' => 587,
              'crlf' => "\r\n",
              'newline' => "\r\n",
              'mailtype'  => 'html'
            ));

            $this->email->from('no-reply@brushworkcosmetics.com', 'Brushwork');
            $this->email->to('contact@brushworkcosmetics.com');
            $this->email->subject('Brushwork\' Affiliate registered by blogger : '. $profile['first_name'] .' '. $profile['middle_name'] .' '. $profile['last_name']);
            $this->email->message($this->load->view('admin/email/signup_inform', array('full_name' => $profile['first_name'].' '.$profile['middle_name'].' '.$profile['last_name'], 'email' => $email, 'contact_number' => $profile['contact_number'], 'id' => $user_id), true));
            $this->email->send();

            redirect('/signup/completed');
         }

         // if($this->aauth->user_exist_by_email($email)){
         //    $data['errors'] = array('email address existing.');
         //    // $user_id = $this->aauth->get_user_id($mail);
         //    // $this->aauth->update_user($user_id, $email, $pass);
         // }else{
         //    $this->aauth->create_user($email, $pass);
         //    $user_id = $this->aauth->get_user_id($email);
         //    $this->Profile->insert(array_merge(array('user_id' => $user_id), $this->input->post('profile')));
         //    redirect('/signup/completed');
         // }
      }
      
      $this->load->view('frontend/signup', $data);
   }

   public function completed(){
      $this->load->view('frontend/signup_completed');
   }

   public function check_existing_email($email = null){
      print $this->aauth->user_exist_by_email($email);
   }

}
