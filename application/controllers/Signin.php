<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signin extends MY_FrontEndController {

   function __construct() {
      parent::__construct();
      $this->load->helper('assets_helper');
      $this->load->library('Aauth');
      $this->load->css("assets/components/animate.css/animate.min.css");
   }

   public function index() {
      $this->output->set_title('Affiliate Program');
      
      $data = array('errors' => array());

      if($this->input->post()){
         $email = $this->input->post('email');
         $pass = $this->input->post('pass');
         $this->aauth->login($email, $pass);
         if($this->aauth->is_loggedin()){
            if($this->aauth->is_admin()){
               redirect('/dashboard');
            }else{
               redirect('/profiles');
            }
         }else{            
            $data['errors'] = $this->aauth->get_errors_array();
         }
      }
      $this->load->view('frontend/signin', $data);
   }

}
