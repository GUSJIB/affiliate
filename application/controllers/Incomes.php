<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Incomes extends MY_BackEndController {

   function __construct() {
      parent::__construct();
      $this->load->helper('assets_helper');
      $this->load->model('Profile');
      $this->load->model('User');
      $this->load->model('Payment');

      $this->load->css("assets/components/animate.css/animate.min.css");
      $this->load->css("assets/components/iCheck/skins/flat/green.css");
      $this->load->css("assets/components/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css");
      $this->load->css("assets/components/select2/dist/css/select2.min.css");
      $this->load->css("assets/themes/Admin/css/select2-bootstrap.css");
      $this->load->js("assets/components/bootstrap-progressbar/bootstrap-progressbar.min.js");
      $this->load->js("assets/components/iCheck/icheck.min.js");

      $this->load->js("assets/components/datatables.net/js/jquery.dataTables.min.js");
      $this->load->js("assets/components/datatables.net-bs/js/dataTables.bootstrap.min.js");
      $this->load->js("assets/components/datatables.net-buttons/js/dataTables.buttons.min.js");
      $this->load->js("assets/components/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");
      $this->load->js("assets/components/datatables.net-buttons/js/buttons.flash.min.js");
      $this->load->js("assets/components/datatables.net-buttons/js/buttons.html5.min.js");
      $this->load->js("assets/components/datatables.net-buttons/js/buttons.print.min.js");
      $this->load->js("assets/components/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");
      $this->load->js("assets/components/datatables.net-keytable/js/dataTables.keyTable.min.js");
      $this->load->js("assets/components/datatables.net-responsive/js/dataTables.responsive.min.js");
      $this->load->js("assets/components/datatables.net-responsive-bs/js/responsive.bootstrap.js");
      $this->load->js("assets/components/datatables.net-scroller/js/dataTables.scroller.min.js");
      $this->load->js("assets/components/jszip/dist/jszip.min.js");
      $this->load->js("assets/components/pdfmake/build/pdfmake.min.js");
      $this->load->js("assets/components/pdfmake/build/vfs_fonts.js");

      $this->load->js("assets/components/moment/moment.js");
      $this->load->js("assets/themes/Frontend/js/datepicker/daterangepicker.js");
      $this->load->js("assets/components/select2/dist/js/select2.full.js");
      $this->load->js("assets/components/parsleyjs/dist/parsley.min.js");
      $this->load->js("assets/themes/Frontend/js/custom.min.js");
      $this->load->js("assets/themes/Admin/js/datatable.js");
   }

   public function index() {
      $this->output->set_title('Incomes');

      $data['payments'] = $this->Payment->where('user_id', $this->aauth->get_user()->id)->get_all();

      $this->load->view('admin/dashboard/incomes', $data);
   }

}
