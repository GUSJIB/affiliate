<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Commissions extends MY_BackEndController {

   function __construct() {
      parent::__construct();
      $this->load->helper('assets_helper');
      $this->load->model('BwsSalesOrder');
      $this->load->model('CouponCode');

      $this->load->css("assets/components/animate.css/animate.min.css");
      $this->load->css("assets/components/iCheck/skins/flat/green.css");
      $this->load->css("assets/components/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css");
      $this->load->js("assets/components/bootstrap-progressbar/bootstrap-progressbar.min.js");
      $this->load->js("assets/components/iCheck/icheck.min.js");

      $this->load->js("assets/components/datatables.net/js/jquery.dataTables.min.js");
      $this->load->js("assets/components/datatables.net-bs/js/dataTables.bootstrap.min.js");
      $this->load->js("assets/components/datatables.net-buttons/js/dataTables.buttons.min.js");
      $this->load->js("assets/components/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");
      $this->load->js("assets/components/datatables.net-buttons/js/buttons.flash.min.js");
      $this->load->js("assets/components/datatables.net-buttons/js/buttons.html5.min.js");
      $this->load->js("assets/components/datatables.net-buttons/js/buttons.print.min.js");
      $this->load->js("assets/components/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");
      $this->load->js("assets/components/datatables.net-keytable/js/dataTables.keyTable.min.js");
      $this->load->js("assets/components/datatables.net-responsive/js/dataTables.responsive.min.js");
      $this->load->js("assets/components/datatables.net-responsive-bs/js/responsive.bootstrap.js");
      $this->load->js("assets/components/datatables.net-scroller/js/dataTables.scroller.min.js");
      $this->load->js("assets/components/jszip/dist/jszip.min.js");
      $this->load->js("assets/components/pdfmake/build/pdfmake.min.js");
      $this->load->js("assets/components/pdfmake/build/vfs_fonts.js");

      $this->load->js("assets/themes/Admin/js/x-editable.js");
      $this->load->js("assets/components/moment/moment.js");
      $this->load->js("assets/themes/Frontend/js/datepicker/daterangepicker.js");
      $this->load->js("assets/themes/Frontend/js/custom.min.js");
      $this->load->js("assets/themes/Admin/js/script.js");
      $this->load->js("assets/themes/Admin/js/commission.js");
   }

   public function index() {
      $this->output->set_title('Commissions');

      $code = $this->CouponCode->where('user_id', $this->aauth->get_user()->id)->get();
      $data['blogger'] = $this->Profile->where('user_id', $this->aauth->get_user()->id)->get();
/*
      $data['orders'] = $this->BwsSalesOrder->get_by_code($code['code']);

      $data['earning_today'] = 0;
      $data['earning_yesterday'] = 0;
      $data['earning_this_week'] = 0;
      $data['earning_this_month'] = 0;
      $data['earning_last_month'] = 0;
*/
	if(empty($code['code'])){
      $data['orders'] = array();

      $data['earning_today'] = 0;   
      $data['earning_overall'] = 0;
      $data['earning_this_week'] = 0;
      $data['earning_this_month'] = 0;
      $data['earning_last_month'] = 0;

      $data['sale_today'] = 0;
      $data['sale_overall'] = 0;
      $data['sale_this_week'] = 0;
      $data['sale_this_month'] = 0;
      $data['sale_last_month'] = 0;
   }else{
      $data['orders'] = $this->BwsSalesOrder->get_by_code($code['code']);

      // Commissions
      $data['earning_today'] = $this->BwsSalesOrder->summary($code['code'], date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59:99'))['commissions'];

      $data['earning_overall'] = $this->BwsSalesOrder->summary_overall($code['code'])['commissions'];

      $day = date('w');
      $week_start = date('Y-m-d 00:00:00', strtotime('-'.$day.' days'));
      $week_end = date('Y-m-d 23:59:59', strtotime('+'.(6-$day).' days'));
      $data['earning_this_week'] = $this->BwsSalesOrder->summary($code['code'], $week_start, $week_end)['commissions'];

      $month_start = date('Y-m-d 00:00:00', strtotime('first day of this month'));
      $month_end  = date('Y-m-d 23:59:59', strtotime('last day of this month'));
      $data['earning_this_month'] = $this->BwsSalesOrder->summary($code['code'], $month_start, $month_end)['commissions'];

      $last_month_start = date('Y-m-d 00:00:00', strtotime('first day of -1 month'));
      $last_month_end = date('Y-m-d 23:59:59', strtotime('last day of -1 month'));
      $data['earning_last_month'] = $this->BwsSalesOrder->summary($code['code'], $last_month_start, $last_month_end)['commissions'];

      // Sales
      $data['sale_today'] = $this->BwsSalesOrder->summary($code['code'], date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59:99'))['sales'];

      $data['sale_overall'] = $this->BwsSalesOrder->summary_overall($code['code'])['sales'];

      $day = date('w');
      $week_start = date('Y-m-d 00:00:00', strtotime('-'.$day.' days'));
      $week_end = date('Y-m-d 23:59:59', strtotime('+'.(6-$day).' days'));
      $data['sale_this_week'] = $this->BwsSalesOrder->summary($code['code'], $week_start, $week_end)['sales'];

      $month_start = date('Y-m-d 00:00:00', strtotime('first day of this month'));
      $month_end  = date('Y-m-d 23:59:59', strtotime('last day of this month'));
      $data['sale_this_month'] = $this->BwsSalesOrder->summary($code['code'], $month_start, $month_end)['sales'];

      $last_month_start = date('Y-m-d 00:00:00', strtotime('first day of -1 month'));
      $last_month_end = date('Y-m-d 23:59:59', strtotime('last day of -1 month'));
      $data['sale_last_month'] = $this->BwsSalesOrder->summary($code['code'], $last_month_start, $last_month_end)['sales'];
   }

      $this->load->view('admin/dashboard/commissions', $data);
   }

   public function view($id) {

      if(!$this->aauth->is_admin()){
         redirect('/');
      }

      $this->output->set_title('Commissions');
      
      $data['blogger'] = $this->Profile->where('user_id', $id)->get();

      $code = $this->CouponCode->where('user_id', $id)->get();
      if(empty($code['code'])){
         $data['orders'] = array();
         $data['earning_today'] = 0;
         $data['earning_overall'] = 0;
         $data['earning_this_week'] = 0;
         $data['earning_this_month'] = 0;
         $data['earning_last_month'] = 0;

         $data['sale_today'] = 0;
         $data['sale_overall'] = 0;
         $data['sale_this_week'] = 0;
         $data['sale_this_month'] = 0;
         $data['sale_last_month'] = 0;
      }else{
         $data['orders'] = $this->BwsSalesOrder->get_by_code($code['code']);

         // Commissions
         $data['earning_today'] = $this->BwsSalesOrder->summary($code['code'], date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59:99'))['commissions'];

         $data['earning_overall'] = $this->BwsSalesOrder->summary_overall($code['code'])['commissions'];

         $day = date('w');
         $week_start = date('Y-m-d 00:00:00', strtotime('-'.$day.' days'));
         $week_end = date('Y-m-d 23:59:59', strtotime('+'.(6-$day).' days'));
         $data['earning_this_week'] = $this->BwsSalesOrder->summary($code['code'], $week_start, $week_end)['commissions'];

         $month_start = date('Y-m-d 00:00:00', strtotime('first day of this month'));
         $month_end  = date('Y-m-d 23:59:59', strtotime('last day of this month'));
         $data['earning_this_month'] = $this->BwsSalesOrder->summary($code['code'], $month_start, $month_end)['commissions'];

         $last_month_start = date('Y-m-d 00:00:00', strtotime('first day of -1 month'));
         $last_month_end = date('Y-m-d 23:59:59', strtotime('last day of -1 month'));
         $data['earning_last_month'] = $this->BwsSalesOrder->summary($code['code'], $last_month_start, $last_month_end)['commissions'];

         // Sales
         $data['sale_today'] = $this->BwsSalesOrder->summary($code['code'], date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59:99'))['sales'];

         $data['sale_overall'] = $this->BwsSalesOrder->summary_overall($code['code'])['sales'];

         $day = date('w');
         $week_start = date('Y-m-d 00:00:00', strtotime('-'.$day.' days'));
         $week_end = date('Y-m-d 23:59:59', strtotime('+'.(6-$day).' days'));
         $data['sale_this_week'] = $this->BwsSalesOrder->summary($code['code'], $week_start, $week_end)['sales'];

         $month_start = date('Y-m-d 00:00:00', strtotime('first day of this month'));
         $month_end  = date('Y-m-d 23:59:59', strtotime('last day of this month'));
         $data['sale_this_month'] = $this->BwsSalesOrder->summary($code['code'], $month_start, $month_end)['sales'];

         $last_month_start = date('Y-m-d 00:00:00', strtotime('first day of -1 month'));
         $last_month_end = date('Y-m-d 23:59:59', strtotime('last day of -1 month'));
         $data['sale_last_month'] = $this->BwsSalesOrder->summary($code['code'], $last_month_start, $last_month_end)['sales'];
      }

      $this->load->view('admin/dashboard/commissions', $data);
   }

}
