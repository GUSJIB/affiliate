<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bloggers extends MY_BackEndController {

   function __construct() {
      parent::__construct();
      $this->load->helper('assets_helper');
      $this->load->model('User');
      $this->load->model('Profile');
      $this->load->model('CouponCode');

      $this->load->css("assets/components/animate.css/animate.min.css");
      $this->load->css("assets/components/iCheck/skins/flat/green.css");
      $this->load->css("assets/components/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css");
      $this->load->js("assets/components/bootstrap-progressbar/bootstrap-progressbar.min.js");
      $this->load->css("assets/components/select2/dist/css/select2.min.css");
      $this->load->css("assets/themes/Admin/css/select2-bootstrap.css");
      $this->load->js("assets/components/iCheck/icheck.min.js");

      $this->load->js("assets/components/datatables.net/js/jquery.dataTables.min.js");
      $this->load->js("assets/components/datatables.net-bs/js/dataTables.bootstrap.min.js");
      $this->load->js("assets/components/datatables.net-buttons/js/dataTables.buttons.min.js");
      $this->load->js("assets/components/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");
      $this->load->js("assets/components/datatables.net-buttons/js/buttons.flash.min.js");
      $this->load->js("assets/components/datatables.net-buttons/js/buttons.html5.min.js");
      $this->load->js("assets/components/datatables.net-buttons/js/buttons.print.min.js");
      $this->load->js("assets/components/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");
      $this->load->js("assets/components/datatables.net-keytable/js/dataTables.keyTable.min.js");
      $this->load->js("assets/components/datatables.net-responsive/js/dataTables.responsive.min.js");
      $this->load->js("assets/components/datatables.net-responsive-bs/js/responsive.bootstrap.js");
      $this->load->js("assets/components/datatables.net-scroller/js/dataTables.scroller.min.js");
      $this->load->js("assets/components/jszip/dist/jszip.min.js");
      $this->load->js("assets/components/pdfmake/build/pdfmake.min.js");
      $this->load->js("assets/components/pdfmake/build/vfs_fonts.js");

      $this->load->js("assets/components/moment/moment.js");
      $this->load->js("assets/themes/Frontend/js/datepicker/daterangepicker.js");
      $this->load->js("assets/components/select2/dist/js/select2.full.js");
      $this->load->js("assets/components/parsleyjs/dist/parsley.min.js");
      $this->load->js("assets/themes/Frontend/js/custom.min.js");
      $this->load->js("assets/themes/Admin/js/datatable.js");
      $this->load->js("assets/themes/Admin/js/payment.js");
      $this->load->js("assets/themes/Admin/js/blogger.js");

      if(!$this->aauth->is_admin()){
        redirect('/');
      }
   }

   public function index() {
      $this->output->set_title('Blogger Lists');

      $data['bloggers'] = $this->User->with_profile()->with_coupon_code()->get_all();
      $data['users'] = $this->User->with_profile()->get_all();

      $this->load->view('admin/dashboard/bloggers', $data);
   }

   public function delete($profile_id = null) {
      if(!$this->aauth->is_admin() || empty($profile_id)){
        redirect('/');
      }

      $this->Profile->delete($profile_id);
      redirect('/bloggers');
   }

}
