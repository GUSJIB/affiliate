<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lost_Password extends MY_FrontEndController {

   function __construct() {
      parent::__construct();
      $this->load->helper('assets_helper');
      $this->load->library('Aauth');
      $this->load->css("assets/components/animate.css/animate.min.css");
   }

   public function index() {
      $this->output->set_title('Lost password');
      
      $data['errors'] = array();
      $data['success'] = array();

      if($this->input->post()){

         if($this->aauth->user_exist_by_email($this->input->post('email'))){
            $this->aauth->remind_password($this->input->post('email'));
            $data['success'] = array('Sent a verification code to given email complated');
         }else{
            $data['errors'] = array('Email doesn\'t existing');
         }
         
      }

      $this->load->view('frontend/lost_password', $data);
   }

}
