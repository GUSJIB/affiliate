<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Password extends MY_BackEndController {

  function __construct() {
    parent::__construct();
    $this->load->helper('assets_helper');
    $this->load->model('User');
    $this->load->model('Profile');
    $this->load->model('CouponCode');
    $this->load->model('Payment');

    $this->load->css("assets/components/animate.css/animate.min.css");
    $this->load->css("assets/components/iCheck/skins/flat/green.css");
    $this->load->css("assets/components/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css");
    $this->load->css("assets/components/select2/dist/css/select2.min.css");
    $this->load->css("assets/themes/Admin/css/select2-bootstrap.css");
    $this->load->js("assets/components/bootstrap-progressbar/bootstrap-progressbar.min.js");
    $this->load->js("assets/components/iCheck/icheck.min.js");
    $this->load->js("assets/components/validator/validator.js");
    $this->load->js("assets/themes/Frontend/js/custom.min.js");
    $this->load->js("assets/themes/Admin/js/password.js");
  }

  public function index() {
    $this->output->set_title('Change Password');
    
    $data['errors'] = array();
    $data['success'] = array();
    
    if($this->input->post()){
      $pass = $this->aauth->hash_password($this->input->post('password'), $this->aauth->get_user()->id);
      // $this->User->update(array('password' => $pass), $this->aauth->get_user()->id);
      $this->db->where('id', $this->aauth->get_user()->id)->update('aauth_users', array('pass' => $pass));
      $data['success'] = array('Change password successfully');
    }

    $this->load->view('admin/dashboard/password', $data);
  }

}
