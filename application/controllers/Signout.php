<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signout extends MY_BackEndController {

   function __construct() {
      parent::__construct();
      $this->load->library('Aauth');
   }

   public function index() {
      $this->aauth->logout();
      redirect('/');
   }

}
