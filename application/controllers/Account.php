<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends MY_FrontEndController {

   function __construct() {
      parent::__construct();
      $this->load->library("Aauth");
      $this->load->helper('assets_helper');
      $this->load->model('User');
   }

   public function verification($user = null, $code = null) {
      if(empty($user) || empty($code)){
         redirect('/');
      }
      $this->output->set_title('Account Verification');
      $this->aauth->verify_user($user, $code);
      $this->load->view('frontend/verification');
   }

   public function reset_password($ver_code = null) {
      if(empty($ver_code)){
         redirect('/');
      }

      $this->aauth->reset_password($ver_code);

      $this->output->set_title('Reset Password');
      $this->load->view('frontend/reset_password');  
   }

}
