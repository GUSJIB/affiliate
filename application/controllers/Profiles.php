<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profiles extends MY_BackEndController {

  function __construct() {
    parent::__construct();
    $this->load->helper('assets_helper');
    $this->load->model('Profile');
    $this->load->model('CouponCode');

    $this->load->css("assets/components/animate.css/animate.min.css");
    $this->load->css("assets/components/iCheck/skins/flat/green.css");
    $this->load->css("assets/components/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css");
    $this->load->css("assets/themes/Admin/css/x-editable.css");
    $this->load->css("assets/components/bootstrap-datepicker/css/bootstrap-datepicker.min.css");
    $this->load->css("assets/components/select2/dist/css/select2.min.css");
    $this->load->css("assets/themes/Admin/css/select2-bootstrap.css");
    $this->load->css("assets/components/typeaheadjs/lib/typeahead.js-bootstrap.css");
    $this->load->js("assets/components/bootstrap-progressbar/bootstrap-progressbar.min.js");
    $this->load->js("assets/components/iCheck/icheck.min.js");
    $this->load->js("assets/themes/Frontend/js/custom.min.js");
    $this->load->js("assets/themes/Admin/js/x-editable.js");
    $this->load->js("assets/components/typeaheadjs/lib/typeahead.js");
    $this->load->js("assets/components/typeaheadjs/typeaheadjs.js");
    $this->load->js("assets/components/bootstrap-datepicker/js/bootstrap-datepicker.min.js");
    $this->load->js("assets/components/moment/moment.js");
    $this->load->js("assets/components/select2/dist/js/select2.full.js");
    $this->load->js("assets/themes/Admin/js/script.js");
  }

  public function index() {
    $this->output->set_title('Affiliate Program');
    $data = $this->Profile->where('user_id', $this->aauth->get_user()->id)->get();
    $data['coupon'] = $this->CouponCode->where('user_id', $data['user_id'])->get();
    $this->load->view('admin/dashboard/profile', $data);
  }

  public function update() {
    if($this->input->post()){
      if($this->aauth->is_admin()){
        $user_profile = $this->Profile->where('user_id', $this->input->post('pk'))->get();
        $this->Profile->update(array($this->input->post('name') => $this->input->post('value')), $user_profile['id']);
      }else{
        $user_profile = $this->Profile->where('user_id', $this->aauth->get_user()->id)->get();
        $this->Profile->update(array($this->input->post('name') => $this->input->post('value')), $user_profile['id']);
      }
      exit;
    }
  }

  public function update_coupon() {
    if($this->input->post() && $this->aauth->is_admin()){
      $user = $this->CouponCode->where('user_id', $this->input->post('pk'))->get();
      if(empty($user)){
        $data = array('user_id' => $this->input->post('pk'), 'code' => $this->input->post('value'));
        $this->CouponCode->insert($data);
      }else{
        $data = array('code' => $this->input->post('value'));
        $this->CouponCode->update($data, $user['id']);
      }

      $u = $this->aauth->get_user($this->input->post('pk'));
      $profile = $this->Profile->where('user_id', $this->input->post('pk'))->get();

      // Imprementation Send Email update Coupon code
      $this->load->library('email');
      $this->email->initialize(array(
        'protocol' => 'smtp',
        'smtp_host' => 'smtp.sendgrid.net',
        'smtp_user' => 'brushwork8',
        'smtp_pass' => 'cosmetics1234',
        'smtp_port' => 587,
        'crlf' => "\r\n",
        'newline' => "\r\n",
        'mailtype'  => 'html'
      ));

      $this->email->from('no-reply@brushworkcosmetics.com', 'Brushwork');
      $to = $u->email;
      $this->email->to($to);
      $this->email->cc('contact@brushworkcosmetics.com');
      $this->email->subject('Brushwork\' Affiliate Coupon Code');
      $this->email->message($this->load->view('admin/email/updated_coupon_code', array('full_name' => $profile['first_name'].' '.$profile['middle_name'].' '.$profile['last_name'], 'code' => $this->input->post('value')), true));
      $this->email->send();
    }
    exit;
  }

  public function view($id = null){
    if(!$this->aauth->is_admin() || empty($id)){
      redirect('/dashboard');
    }
    
    $data = $this->Profile->where('user_id', $id)->get();
    $data['coupon'] = $this->CouponCode->where('user_id', $data['user_id'])->get();
    $this->load->view('admin/dashboard/profile', $data);
  }

  public function set_admin($user_id) {
    if(!$this->aauth->is_admin() || empty($user_id)){
      redirect('/dashboard');
    }
    $this->aauth->add_member($user_id, 'admin');
    redirect('/bloggers');
  }

  public function remove_admin($user_id) {
    if(!$this->aauth->is_admin() || empty($user_id)){
      redirect('/dashboard');
    }
    $this->aauth->remove_member($user_id, 'admin');
    redirect('/bloggers');
  }

  public function resend_verify($user_id) {
    if(!$this->aauth->is_admin() || empty($user_id)){
      redirect('/dashboard');
    }

    $this->aauth->send_verification($user_id);
    redirect('/bloggers');
  }

  public function getCodes(){
    if(!$this->aauth->is_admin()){
      exit;
    }

    $this->load->model('BwsSaleruleCoupon');
    $result = $this->CouponCode->fields('code')->get_all();
    $used = array();
    foreach ($result as $value) {
      array_push($used, $value['code']);
    }
    print json_encode($this->BwsSaleruleCoupon->code_available($used));
    exit;
  }

}
