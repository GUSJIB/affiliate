<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends MY_Model {

   public function __construct()
   {
      $this->has_one['user'] = array('foreign_model'=>'User','foreign_table'=>'aauth_users','foreign_key'=>'id','local_key'=>'user_id');
      $this->has_one['profile'] = array('foreign_model'=>'Profile','foreign_table'=>'profiles','foreign_key'=>'user_id','local_key'=>'user_id');
      parent::__construct();
   }

   public $table = 'payments';

}