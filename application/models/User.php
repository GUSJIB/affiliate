<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Model {

   public function __construct()
   {
      $this->has_one['profile'] = array('foreign_model'=>'Profile','foreign_table'=>'profiles','foreign_key'=>'user_id','local_key'=>'id');
      $this->has_many['coupon_code'] = array('foreign_model'=>'CouponCode','foreign_table'=>'coupon_codes','foreign_key'=>'user_id','local_key'=>'id');
      parent::__construct();
   }
   
   public $table = 'aauth_users';

   public function groups_to_string($obj){
      $data = array();
      foreach ($obj as $key => $value) {
         array_push($data, $value->name == 'Default' ? 'Blogger' : $value->name);
      }
      return implode(',', $data);
   }
}
