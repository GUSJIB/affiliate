<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Model {

   public $table = 'profiles';
   public function __construct()
   {
      parent::__construct();
      $this->soft_deletes = TRUE;
   }

}
