<?php defined('BASEPATH') OR exit('No direct script access allowed');

class BwsSalesOrder extends CI_Model
{

  private $_database;
  
  private $_table = 'bws_sales_order';

  public function __construct(){
     parent::__construct();
     $this->_database = $this->load->database('brushwork', TRUE);
  }

  public function get_by_code($code) {
    $fields = "entity_id, order_currency_code, status, coupon_code, discount_amount, subtotal_incl_tax, grand_total, shipping_amount, updated_at";
    $where = array(
      'status' => 'complete',
      'coupon_code' => $code
    );
    return $this->_database->select($fields)->where($where)->get($this->_table)->result_array();
  }

  // Summary Dashboard
  public function summary_dashboard($date_from, $date_to) {
    $fields = "entity_id, order_currency_code, status, coupon_code, discount_amount, subtotal_incl_tax, grand_total, shipping_amount, updated_at";
    $where = array(
      'status' => 'complete',
      'coupon_code !=' => '',
      'updated_at >=' => $date_from,
      'updated_at <=' => $date_to
    );

    $data = $this->_database->select($fields)->where($where)->get($this->_table)->result_array();

    $commissions = 0;
    $sales = 0;
    foreach ($data as $key => $value) {
      $commissions = $commissions + ((($value['grand_total'] - $value['shipping_amount']) * 15) / 100);
      $sales = $sales + ($value['grand_total'] - $value['shipping_amount']);
    }

    return array('commissions' => $commissions, 'sales' => $sales);
  }

  // Summary Dashboard Overall
  public function summary_dashboard_overall() {
    $fields = "entity_id, order_currency_code, status, coupon_code, discount_amount, subtotal_incl_tax, grand_total, shipping_amount, updated_at";
    $where = array(
      'status' => 'complete',
      'coupon_code !=' => ''
    );

    $data = $this->_database->select($fields)->where($where)->get($this->_table)->result_array();

    $commissions = 0;
    $sales = 0;
    foreach ($data as $key => $value) {
      $commissions = $commissions + ((($value['grand_total'] - $value['shipping_amount']) * 15) / 100);
      $sales = $sales + ($value['grand_total'] - $value['shipping_amount']);
    }

    return array('commissions' => $commissions, 'sales' => $sales);
  }

  // Summary Commissions
  public function summary($code, $date_from, $date_to) {
    $fields = "entity_id, order_currency_code, status, coupon_code, discount_amount, subtotal_incl_tax, grand_total, shipping_amount, updated_at";
    $where = array(
      'status' => 'complete',
      'coupon_code' => $code,
      'updated_at >=' => $date_from,
      'updated_at <=' => $date_to
    );

    $data = $this->_database->select($fields)->where($where)->get($this->_table)->result_array();

    $commissions = 0;
    $sales = 0;
    foreach ($data as $key => $value) {
      $commissions = $commissions + ((($value['grand_total'] - $value['shipping_amount']) * 15) / 100);
      $sales = $sales + ($value['grand_total'] - $value['shipping_amount']);
    }

    return array('commissions' => $commissions, 'sales' => $sales);
  }

  // Summary Commissions Overall
  public function summary_overall($code) {
    $fields = "entity_id, order_currency_code, status, coupon_code, discount_amount, subtotal_incl_tax, grand_total, shipping_amount, updated_at";
    $where = array(
      'status' => 'complete',
      'coupon_code' => $code
    );

    $data = $this->_database->select($fields)->where($where)->get($this->_table)->result_array();

    $commissions = 0;
    $sales = 0;
    foreach ($data as $key => $value) {
      $commissions = $commissions + ((($value['grand_total'] - $value['shipping_amount']) * 15) / 100);
      $sales = $sales + ($value['grand_total'] - $value['shipping_amount']);
    }

    return array('commissions' => $commissions, 'sales' => $sales);
  }

  public function get_by_all_code() {
    $fields = "entity_id, order_currency_code, status, coupon_code, discount_amount, subtotal_incl_tax, grand_total, shipping_amount, updated_at";
    $where = array(
      'status' => 'complete',
      'coupon_code !=' => '' 
    );
    return $this->_database->select($fields)->where($where)->get($this->_table)->result_array();
  }

  // public function get_by_date($code, $date_from, $date_to) {
  //   $fields = "entity_id, order_currency_code, status, coupon_code, discount_amount, subtotal_incl_tax, grand_total, shipping_amount, updated_at";
  //   $where = array(
  //     'status' => 'complete',
  //     'coupon_code' => $code
  //   );
  //   return $this->_database->select($fields)->where($where)->get($this->_table)->result_array();
  // }

}
