<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Model {

   public function __construct(){
      parent::__construct();
   }

   public function build_menu($is_admin = false){
      $menus = array();
      if($is_admin){
         array_push($menus, array('icon' => 'home', 'name' => 'Dashboard', 'url' => '/dashboard'));
         array_push($menus, array('icon' => 'user', 'name' => 'Profile', 'url' => '/profiles'));
         array_push($menus, array('icon' => 'users', 'name' => 'Bloggers', 'url' => '/bloggers'));
         array_push($menus, array('icon' => 'ticket', 'name' => 'Payments', 'url' => '/payments'));
         array_push($menus, array('icon' => 'envelope', 'name' => 'Newsletters', 'url' => '/newsletters'));
      }else{
         array_push($menus, array('icon' => 'user', 'name' => 'Profile', 'url' => '/profiles'));
         array_push($menus, array('icon' => 'line-chart', 'name' => 'Commissions', 'url' => '/commissions'));
         array_push($menus, array('icon' => 'money', 'name' => 'Incomes', 'url' => '/incomes'));
      }
      return $menus;
   }
}