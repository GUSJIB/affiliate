<?php defined('BASEPATH') OR exit('No direct script access allowed');

class BwsSaleruleCoupon extends CI_Model
{
   private $_database;
  
   private $_table = 'bws_salesrule_coupon';

   public function __construct(){
      parent::__construct();
      $this->_database = $this->load->database('brushwork', TRUE);
   }

   public function code_available($used = array()) {
      $data = $this->_database->where_not_in('code', $used)->select('code')->get($this->_table)->result_array();
      return $data;
   }

}
