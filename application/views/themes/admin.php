<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $title; ?></title>

    <!-- Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1.0" />
    <!-- <meta name="robots" content="index, follow" /> -->
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="copyright" content="" />

    <?php
      if(!empty($meta)){
         foreach($meta as $name=>$content){
            echo "\n\t\t";
            ?><meta name="<?php echo $name; ?>" content="<?php echo $content; ?>" /><?php
         } echo "\n";
      }

      if(!empty($canonical)) {
         echo "\n\t\t";
         ?><link rel="canonical" href="<?php echo $canonical?>" /><?php
      }
      echo "\n\t";
    ?>

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="" />

    <!-- Material Design fonts -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">

    <!-- CSS -->
    <?php
      foreach($css as $file){
        echo "\n\t\t";
        ?><link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
      } echo "\n\t";

    ?>

    <!--[if lte IE 7]><script src="<?= asset_path('assets/themes/Frontend/lte-ie7.js') ?>"></script><![endif]-->

    <!--[if IE 9]>
      <link rel="stylesheet" type="text/css" href="<?= asset_path('assets/themes/Frontend/css/ie9.css') ?>">
    <![endif]-->
  </head>

  <body class="nav-md">

    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="/dashboard" class="site_title"><img width="200px" src="/assets/themes/Frontend/images/logo-white.png" alt="Brushwork"></a>
            </div>

            <div class="clearfix"></div>

            <!-- sidebar menu -->
              <?php echo $this->load->get_section('sidebar'); ?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <?php echo $this->load->get_section('topright'); ?>
                </li>

                <li role="presentation" class="dropdown">
                  <?php echo $this->load->get_section('messages'); ?>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <?php echo $output; ?>
        <!-- /page content -->

      </div>
    </div>

    <!-- footer content -->
    <footer>
      <div class="pull-right">
        COPYRIGHT © 2016 BRUSHWORK. ALL RIGHTS RESERVED.
      </div>
      <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->

    <!-- JS -->
    <?php
       foreach($js as $file){
          echo "\n\t\t";
          ?><script src="<?php echo $file; ?>"></script><?php
       } echo "\n\t";
    ?>
  </body>
</html>