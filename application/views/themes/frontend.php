<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>

    <title><?php echo $title; ?></title>

    <!-- Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1.0" />
    <meta name="robots" content="index, follow" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="copyright" content="" />

    <?php
      if(!empty($meta)){
         foreach($meta as $name=>$content){
            echo "\n\t\t";
            ?><meta name="<?php echo $name; ?>" content="<?php echo $content; ?>" /><?php
         } echo "\n";
      }

      if(!empty($canonical)) {
         echo "\n\t\t";
         ?><link rel="canonical" href="<?php echo $canonical?>" /><?php
      }
      echo "\n\t";
    ?>

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="" />

    <!-- Material Design fonts -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">

    <!-- CSS -->
    <?php
      foreach($css as $file){
        echo "\n\t\t";
        ?><link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
      } echo "\n\t";

    ?>

    <!--[if lte IE 7]><script src="<?= asset_path('assets/themes/Frontend/lte-ie7.js') ?>"></script><![endif]-->

    <!--[if IE 9]>
      <link rel="stylesheet" type="text/css" href="<?= asset_path('assets/themes/Frontend/css/ie9.css') ?>">
    <![endif]-->

  </head>

  <body class="nav-md">

    <header>
      <div class="header content">
        <a class="logo" rel="home" href="/" title="Brushwork">
          <img src="/assets/themes/Frontend/images/logo-white.png" alt="Brushwork">
        </a>
      </div>
    </header>

    <div class="container body">
      <div class="main_container">

        <!-- page content -->
        <?php echo $output; ?>
        <!-- /page content -->

        
      </div>
    </div>

    <!-- footer content -->
    <footer style="margin-left: 0px;">
      <div class="pull-right" style="color: azure;">
        COPYRIGHT © 2016 BRUSHWORK. ALL RIGHTS RESERVED.
      </div>
      <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->

      
    <!-- JS -->
    <?php
       foreach($js as $file){
          echo "\n\t\t";
          ?><script src="<?php echo $file; ?>"></script><?php
       } echo "\n\t";
    ?>
      
  </body>
</html>