<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Bloggers</h3>
      </div>

      <div class="title_right">
        
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content">
            
            <table id="datatable-buttons" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Coupon Code</th>
                  <th>Group</th>
                  <th>Verification</th>
                  <th>Registered date</th>
                  <th>Action</th>
                </tr>
              </thead>

              <tbody>
                <? $bloggers = empty($bloggers) ? array() : $bloggers  ?>
                <? foreach($bloggers as $blogger): ?>
                  <? if(array_key_exists("profile",$blogger)): ?>
                    <tr>
                      <td><?= $blogger['profile']['first_name']. ' ' .$blogger['profile']['last_name'] ?></td>
                      <td><?= $blogger['email'] ?></td>
                      <td><?= array_key_exists("coupon_code",$blogger) ? $blogger['coupon_code'][0]['code'] : '' ?></td>
                      <td><?= $this->User->groups_to_string($this->aauth->get_user_groups($blogger['id'])) ?></td>
                      <td>
                        <? if(!$this->aauth->is_banned($blogger['id'])): ?>
                          <span class="green">Verified</span>
                        <? else: ?>
                          <span class="red">Not Verified</span>
                        <? endif; ?>
                      </td>
                      <td><?= $blogger['date_created'] ?></td>
                      <td>
                        <div class="btn-group">
                          <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu">
                            <li><a href="/profiles/view/<?= $blogger['id'] ?>">View Detail</a></li>
                            <li><a href="/commissions/view/<?= $blogger['id'] ?>">Commission</a></li>
                            <li><a href="#" class="add-payment" data-value="<?= $blogger['id'] ?>">Add Payment</a></li>
                            <li>
                              <? $str = $this->User->groups_to_string($this->aauth->get_user_groups($blogger['id'])); ?>
                              <? if(preg_match('/Admin/',$str)): ?>
                                <a href="/profiles/remove_admin/<?= $blogger['id'] ?>">Set to Blogger only</a>
                              <? else: ?>
                                <a href="/profiles/set_admin/<?= $blogger['id'] ?>">Set To Admin</a>
                              <? endif; ?>
                            </li>
                            <li><a href="/bloggers/delete/<?= $blogger['profile']['id'] ?>" onclick="return confirm('Are you sure to delete \'<?= $blogger['profile']['first_name']. ' ' .$blogger['profile']['last_name'] ?>\'?')">Delete</a></li>
                            <li><a href="/profiles/resend_verify/<?= $blogger['id'] ?>">Re-send verify email</a></li>
                          </ul>
                        </div>
                      </td>
                    </tr>
                  <? endif; ?>
                <? endforeach; ?>
              </tbody>
            </table>

          </div>
        </div>        
      </div>
    </div>

  </div>
</div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <form action="/payments/add" method="post" id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Add Payment</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
              <!-- <div class="x_title">
                <h2>Form Design <small>different form elements</small></h2>
              </div> -->
              <div class="x_content">
                
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="recipient-name">Recipient Name <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <select id="recipient-name" class="select2_single form-control" name="user_id" required="required">
                      <option></option>
                      <? foreach($users as $user): ?>
                        <? if(array_key_exists("profile",$user)): ?>
                          <option value="<?= $user['id'] ?>"><?= $user['profile']['first_name']. ' ' .$user['profile']['last_name'] ?></option>
                        <? endif; ?>
                      <? endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">Amount <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="amount" name="amount" data-parsley-validation-threshold="0" data-parsley-trigger="keyup" data-parsley-type="number" required="required" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="paid-date">Paid date <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="paid-date" class="date-picker form-control col-md-7 col-xs-12" name="paid_at" pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])" required="required" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="paid-date">Remarks</label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <textarea name="remarks" class="form-control col-md-7 col-xs-12"></textarea>
                  </div>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit Payment</button>
      </div>

    </div>
    </form>
  </div>
</div>