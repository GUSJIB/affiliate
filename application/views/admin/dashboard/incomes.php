<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Incomes</h3>
      </div>

      <div class="title_right">
        
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content">
            
            <table id="datatable-buttons" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Amount</th>
                  <th>Paid date</th>
                </tr>
              </thead>

              <tbody>
                <? $payments = empty($payments) ? array() : $payments  ?>
                <? foreach($payments as $pay): ?>
                  <tr>
                    <td><?= $pay['amount'] ?></td>
                    <td><?= $pay['paid_at'] ?></td>
                  </tr>
                <? endforeach; ?>
              </tbody>
            </table>

          </div>
        </div>        
      </div>
    </div>

  </div>
</div>
