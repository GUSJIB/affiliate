<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Earnings <small><?= $blogger['first_name']. ' ' .$blogger['last_name'] ?></small></h3>
      </div>

      <div class="title_right">
        <div class="col-md-6 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>

    <!-- top tiles -->
    <div class="row tile_count">
      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><h2 class="red">Commissions</h2></span>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-money"></i> Today</span>
        <div class="count green"><?= number_format($earning_today, 2) ?></div>
        <span class="count_bottom"> THB</span>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-money"></i> This Week</span>
        <div class="count green"><?= number_format($earning_this_week, 2) ?></div>
        <span class="count_bottom"> THB</span>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-money"></i> This Month</span>
        <div class="count green"><?= number_format($earning_this_month, 2) ?></div>
        <span class="count_bottom"> THB</span>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-money"></i> Last Month</span>
        <div class="count green"><?= number_format($earning_last_month, 2) ?></div>
        <span class="count_bottom"> THB</span>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-money"></i> Overall</span>
        <div class="count green"><?= number_format($earning_overall, 2) ?></div>
        <span class="count_bottom"> THB</span>
      </div>
    </div>
    <!-- /top tiles -->

    <!-- top tiles -->
    <div class="row tile_count">
      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><h2 class="red">Total Orders</h2></span>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-money"></i> Today</span>
        <div class="count green"><?= number_format($sale_today, 2) ?></div>
        <span class="count_bottom"> THB</span>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-money"></i> This Week</span>
        <div class="count green"><?= number_format($sale_this_week, 2) ?></div>
        <span class="count_bottom"> THB</span>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-money"></i> This Month</span>
        <div class="count green"><?= number_format($sale_this_month, 2) ?></div>
        <span class="count_bottom"> THB</span>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-money"></i> Last Month</span>
        <div class="count green"><?= number_format($sale_last_month, 2) ?></div>
        <span class="count_bottom"> THB</span>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-money"></i> Overall</span>
        <div class="count green"><?= number_format($sale_overall, 2) ?></div>
        <span class="count_bottom"> THB</span>
      </div>
    </div>
    <!-- /top tiles -->

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><i class="fa fa-bars"></i> Orders <small></small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li>
                <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                <span></span> <b class="caret"></b>
            </div>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <table id="datatable" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Order ID</th>
                  <th>Order Date</th>
                  <th>Coupon Code</th>
                  <th>Status</th>
                  <th>Amount</th>
                  <th>Commission</th>
                  <th>Currency</th>
                </tr>
              </thead>

              <tbody>
                <? foreach($orders as $order): ?>
                  <tr>
                    <td>#<?= $order['entity_id'] ?></td>
                    <td><?= $order['updated_at'] ?></td>
                    <td><?= $order['coupon_code'] ?></td>
                    <td><?= $order['status'] ?></td>
                    <td><?= number_format($order['grand_total'] - $order['shipping_amount'], 2) ?></td>
                    <td><?= number_format((($order['grand_total'] - $order['shipping_amount']) * 15) / 100, 2) ?></td>
                    <td><?= $order['order_currency_code'] ?></td>
                  </tr>
                <? endforeach; ?>
              </tbody>
            </table>

          </div>
        </div>
      </div>
    </div>

  </div>
</div>

<style type="text/css">
  .tile_count .tile_stats_count:before {
    border-left: none;
  }
</style>

<form id="search-order">
  <input id="date-from" name="date_from" type="hidden" value="<?=$this->input->get('date_from')?>" />
  <input id="date-to" name="date_to" type="hidden" value="<?=$this->input->get('date_to')?>" />
</form>