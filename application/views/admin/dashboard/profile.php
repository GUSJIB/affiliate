<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Profiles</h3>
      </div>

      <div class="title_right">

      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Account Details <small class="red">*please click on the text to update information</small></h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br>
            <form id="account-details" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
              <div class="row">
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">First Name </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <a href="#" class="col-md-7 col-xs-12 editable" data-type="text" data-pk="<?= $user_id ?>" data-name="first_name" data-url="/profiles/update"><?= $first_name ?></a>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Last Name</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <a href="#" class="col-md-7 col-xs-12 editable" data-type="text" data-pk="<?= $user_id ?>" data-name="last_name" data-url="/profiles/update"><?= $last_name ?></a>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Middle Name / Initial</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <a href="#" class="col-md-7 col-xs-12 editable" data-type="text" data-pk="<?= $user_id ?>" data-name="middle_name" data-url="/profiles/update"><?= $middle_name ?></a>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <a href="#" class="col-md-7 col-xs-12" current-value="<?= $gender ?>" id="gender" data-type="select" data-pk="<?= $user_id ?>" data-name="gender" data-url="/profiles/update"><?= $gender ?></a>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <a href="#" id="dob" data-type="combodate" data-value="<?= $dob ?>" data-pk="1" data-url="/profiles/update" data-name="dob"></a>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Coupon Code</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <? if($this->aauth->is_admin()): ?>
                        <a id="coupon-code" href="#" current-value="<?= $coupon['code'] ?>" class="col-md-7 col-xs-12" data-type="select" data-pk="<?= $user_id ?>" data-name="code" data-url="/profiles/update_coupon"><?= $coupon['code'] ?></a>
                      <? else: ?>
                        <span style="padding-top: 6px;display: inline-block;">
                          <?= empty($coupon['code']) ? 'Please wait administrators are creating coupon code' : $coupon['code'] ?>
                        </span>
                      <? endif; ?>
                    </div>
                  </div>
                  <div class="ln_solid"></div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Address</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <a href="#" class="col-md-7 col-xs-12 editable" data-type="text" data-pk="<?= $user_id ?>" data-name="address" data-url="/profiles/update"><?= $address ?></a>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="postal_code">Postal Code</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <a href="#" class="col-md-7 col-xs-12 editable" data-type="text" data-pk="<?= $user_id ?>" data-name="postal_code" data-url="/profiles/update"><?= $postal_code ?></a>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="country">Country</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <a href="#" class="col-md-7 col-xs-12" current-value="<?= $country ?>" id="country" data-type="select" data-pk="<?= $user_id ?>" data-name="country" data-url="/profiles/update"><?= $country ?></a>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="country_code">Country Code</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <a href="#" class="col-md-7 col-xs-12 editable" data-type="text" data-pk="<?= $user_id ?>" data-name="country_code" data-url="/profiles/update"><?= $country_code ?></a>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="contact_number">Contact Number</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <a href="#" class="col-md-7 col-xs-12 editable" data-type="text" data-pk="<?= $user_id ?>" data-name="contact_number" data-url="/profiles/update"><?= $contact_number ?></a>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Social Network <small class="red">*please click on the text to update information</small></h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br>
            <form class="form-horizontal form-label-left input_mask">

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Blog URL</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <a href="#" class="col-md-7 col-xs-12 editable" data-type="text" data-pk="<?= $user_id ?>" data-name="blog_url" data-url="/profiles/update"><?= $blog_url ?></a>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Facebook</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <a href="#" class="col-md-7 col-xs-12 editable" data-type="text" data-pk="<?= $user_id ?>" data-name="facebook" data-url="/profiles/update"><?= $facebook ?></a>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Instagram</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <a href="#" class="col-md-7 col-xs-12 editable" data-type="text" data-pk="<?= $user_id ?>" data-name="instagram" data-url="/profiles/update"><?= $instagram ?></a>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Youtube</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <a href="#" class="col-md-7 col-xs-12 editable" data-type="text" data-pk="<?= $user_id ?>" data-name="youtube" data-url="/profiles/update"><?= $youtube ?></a>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Snapchat</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <a href="#" class="col-md-7 col-xs-12 editable" data-type="text" data-pk="<?= $user_id ?>" data-name="snapchat" data-url="/profiles/update"><?= $snapchat ?></a>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Twitter</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <a href="#" class="col-md-7 col-xs-12 editable" data-type="text" data-pk="<?= $user_id ?>" data-name="twitter" data-url="/profiles/update"><?= $twitter ?></a>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Other</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <a href="#" class="col-md-7 col-xs-12 editable" data-type="text" data-pk="<?= $user_id ?>" data-name="other" data-url="/profiles/update"><?= $other ?></a>
                </div>
              </div>
              <div class="ln_solid"></div>
            </form>
          </div>
        </div>
      </div>

      <div class="col-md-6 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Payment Details <small class="red">*please click on the text to update information</small></h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br>
            <form class="form-horizontal form-label-left">

              <div class="form-group">
                <label class="control-label col-md-5 col-sm-3 col-xs-12">Recipient Name</label>
                <div class="col-md-7 col-sm-9 col-xs-12">
                  <a href="#" class="col-md-7 col-xs-12 editable" data-type="text" data-pk="<?= $user_id ?>" data-name="recipient_name" data-url="/profiles/update"><?= $recipient_name ?></a>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-5 col-sm-3 col-xs-12">Recipient Bank Name</label>
                <div class="col-md-7 col-sm-9 col-xs-12">
                  <a href="#" class="col-md-7 col-xs-12 editable" data-type="text" data-pk="<?= $user_id ?>" data-name="recipient_bank_name" data-url="/profiles/update"><?= $recipient_bank_name ?></a>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-5 col-sm-3 col-xs-12">Bank Address</label>
                <div class="col-md-7 col-sm-9 col-xs-12">
                  <a href="#" class="col-md-7 col-xs-12 editable" data-type="text" data-pk="<?= $user_id ?>" data-name="bank_address" data-url="/profiles/update"><?= $bank_address ?></a>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-5 col-sm-3 col-xs-12">Bank Country</label>
                <div class="col-md-7 col-sm-9 col-xs-12">
                  <a href="#" class="col-md-7 col-xs-12 editable" data-type="text" data-pk="<?= $user_id ?>" data-name="bank_country" data-url="/profiles/update"><?= $bank_country ?></a>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-5 col-sm-3 col-xs-12">Bank Routing Code</label>
                <div class="col-md-7 col-sm-9 col-xs-12">
                  <a href="#" class="col-md-7 col-xs-12 editable" data-type="text" data-pk="<?= $user_id ?>" data-name="bank_routing_code" data-url="/profiles/update"><?= $bank_routing_code ?></a>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-5 col-sm-3 col-xs-12">Bank Account Number</label>
                <div class="col-md-7 col-sm-9 col-xs-12">
                  <a href="#" class="col-md-7 col-xs-12 editable" data-type="text" data-pk="<?= $user_id ?>" data-name="bank_account_number" data-url="/profiles/update"><?= $bank_account_number ?></a>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-5 col-sm-3 col-xs-12">Paypal Account</label>
                <div class="col-md-7 col-sm-9 col-xs-12">
                  <a href="#" class="col-md-7 col-xs-12 editable" data-type="text" data-pk="<?= $user_id ?>" data-name="paypal_account" data-url="/profiles/update"><?= $paypal_account ?></a>
                </div>
              </div>

              <div class="ln_solid"></div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
