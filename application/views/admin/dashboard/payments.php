<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Payments</h3>
      </div>

      <div class="title_right">
        <div class="pull-right">
          <button class="btn btn-success" data-toggle="modal" data-target=".bs-example-modal-lg">Add Payment</button>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content">
            
            <table id="datatable-buttons" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Recipient Name</th>
                  <th>Amount</th>
                  <th>Paid date</th>
                  <th>Remarks</th>
                </tr>
              </thead>

              <tbody>
                <? $payments = empty($payments) ? array() : $payments  ?>
                <? foreach($payments as $pay): ?>
                  <tr>
                    <td><?= $pay['profile']['first_name']. ' ' .$pay['profile']['last_name'] ?></td>
                    <td><?= $pay['amount'] ?></td>
                    <td><?= $pay['paid_at'] ?></td>
                    <td><?= $pay['remarks'] ?></td>
                  </tr>
                <? endforeach; ?>
              </tbody>
            </table>

          </div>
        </div>        
      </div>
    </div>

  </div>
</div>


<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <form action="/payments/add" method="post" id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Add Payment</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
              <!-- <div class="x_title">
                <h2>Form Design <small>different form elements</small></h2>
              </div> -->
              <div class="x_content">
                
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="recipient-name">Recipient Name <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <select class="select2_single form-control" name="user_id" required="required">
                      <option></option>
                      <? foreach($users as $user): ?>
                        <? if(array_key_exists("profile",$user)): ?>
                          <option value="<?= $user['id'] ?>"><?= $user['profile']['first_name']. ' ' .$user['profile']['last_name'] ?></option>
                        <? endif; ?>
                      <? endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">Amount <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="amount" name="amount" data-parsley-validation-threshold="0" data-parsley-trigger="keyup" data-parsley-type="number" required="required" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="paid-date">Paid date <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="paid-date" class="date-picker form-control col-md-7 col-xs-12" name="paid_at" pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])" required="required" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="paid-date">Remarks</label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <textarea name="remarks" class="form-control col-md-7 col-xs-12"></textarea>
                  </div>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit Payment</button>
      </div>

    </div>
    </form>
  </div>
</div>