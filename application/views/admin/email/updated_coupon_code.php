<b>Dear <?= $full_name ?></b>
<br/>
<div>
   <p>Welcome to BRUSHWORK family!</p> 
   <br/>
   <p>We are so excited to have you as our affiliate. Your coupon code is “<span style="color: blue;"><?= $code ?></span>”</p>
   <br/>
   <p>With this code, your followers/fans will receive a 10% product discount on all orders at</p>
   <p><a href="http://www.brushworkcosmetics.com">http://www.brushworkcosmetics.com</a> and, as our affiliate, you will receive a 15% commission.</p>
   <p><small>*The Conditions are subject by Lifestyle Perfection Co. Ltd.</small></p>
   <br/>
   <p>Log in to check for details and updates at <a href="http://affiliates.brushworkcosmetics.com">affiliates.brushworkcosmetics.com</a></p>
   <br/>
   <p>Should you have any questions, we are always here at your disposal:</p>
   <p>contact@brushworkcosmetics.com</p>
   <br/>
   <p>It is our honor to work with you!</p>
   <p>LOVE</p>
   <br/>
   <p>BRUSHWORK team</p>
</div>