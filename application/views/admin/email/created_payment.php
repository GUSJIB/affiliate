<b>Dear <?= $full_name ?></b>
<br/>
<div>
   <p>Thank you for your great support for BRUSHWORK!</p>
   <p><b>We would like to kindly inform you that a payment for BRUSHWORK’s<br/>affiliates program has just been made to your provided ‘payment details’.</b></p>
   <br/>
   <p>More details can be viewed on ‘Incomes’ page at: <br/><a href="http://affiliates.brushworkcosmetics.com">affiliates.brushworkcosmetics.com</a></p>
   <br/>
   <p>With love,</p>
   <br/>
   <p>BRUSHWORK team</p>
</div>