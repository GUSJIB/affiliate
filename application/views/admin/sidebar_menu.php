<!-- menu profile quick info -->
<div class="profile">
  <div class="profile_pic">
    <img src="/assets/themes/Frontend/images/avatar.png" alt="..." class="img-circle profile_img">
  </div>
  <div class="profile_info">
    <span>Welcome,</span>
    <h2><?= $first_name . ' ' . $last_name ?></h2>
  </div>
</div>
<!-- /menu profile quick info -->

<br />

<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <h3>Menu</h3>
    <ul class="nav side-menu">

      <? foreach ($this->Menu->build_menu($this->aauth->is_admin()) as $menu): ?>
        <li><a href="<?= $menu['url'] ?>"><i class="fa fa-<?= $menu['icon'] ?>"></i> <?= $menu['name'] ?> </a></li>  
      <? endforeach; ?>
      
    </ul>
  </div>

</div>