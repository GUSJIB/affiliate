<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
  <img src="/assets/themes/Frontend/images/avatar.png" alt=""> <?= $first_name . ' ' . $last_name ?>
  <span class=" fa fa-angle-down"></span>
</a>
<ul class="dropdown-menu dropdown-usermenu pull-right">
  <li><a href="/profiles"> Profile</a></li>
  <li><a href="/password"> Change Password</a></li>
  <!-- <li>
    <a href="javascript:;">
      <span class="badge bg-red pull-right">50%</span>
      <span>Settings</span>
    </a>
  </li> -->
  <!-- <li><a href="javascript:;">Help</a></li> -->
  <li><a href="/signout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
</ul>