<div class="login" style="min-height: 550px;">

    <div class="login_wrapper" style="margin-top: auto;">
      <div class="animate form login_form" style="position: inherit;">
        <section class="login_content">
          <form action="" method="post">
            <h1>Lost Password</h1>
            <div>
              <? foreach($errors as $error): ?>
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                  <?= $error ?>
                </div>
              <? endforeach; ?>
              <? foreach($success as $suc): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                  <?= $suc ?>
                </div>
              <? endforeach; ?>
            </div>
            <div>
              <input type="text" name="email" class="form-control" placeholder="Email" required="required">
            </div>
            <div>
              <input type="submit" class="btn btn-default submit" value="Reset Password" />
              <a class="reset_pass" href="/">Sign In</a>
            </div>

            <div class="clearfix"></div>

            <div class="separator">
              <div>
                <h1><i class="fa fa-paint-brush"></i> BRUSHWORK COSMETICS</h1>
              </div>
            </div>
          </form>
        </section>
      </div>
    </div>
</div>
