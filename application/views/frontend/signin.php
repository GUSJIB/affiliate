<link href='https://fonts.googleapis.com/css?family=Permanent+Marker|Montserrat:700' rel='stylesheet' type='text/css'>
<div class="login" style="min-height: 550px;">
  <div style="background-color:rgba(0, 0, 0, 0.7);">
  <div style="font-size: 14px;color: azure;margin:0 auto;max-width:650px;text-align:center;">
    <div style="font-size:18px;font-family: 'Permanent Marker', cursive;">Hi love!
    <br/><br/>
    Welcome to BRUSHWORK affiliate family. 
    </div>
    <br/>
    <div style="font-family: 'Montserrat', sans-serif;">For our affiliates, please log in to track your sales progress.</div>
    <br/>
    <div style="font-family: 'Montserrat', sans-serif;">
    If you are interested in becoming BRUSHWORK affiliate, please click "<a style="color:cornflowerblue;" href="/signup">Create Account</a>" and we will get back to you a.s.a.p.
    By being our affiliates, you will receive our BRUSHWORK premium products, exclusive offers, your followers who've used your code will receive discounts, and you will receive 15% commission on sales.
    <br/><br/>
    Look forward to welcoming you on board!
    </div>
  </div>  
  </div>
    <div class="login_wrapper" style="margin-top: auto;">
      <div class="animate form login_form" style="position: inherit;">
        <section class="login_content">
          <form action="" method="post">
            <h1>Sign In</h1>
            <div>
              <? foreach($errors as $error): ?>
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                  <?= $error ?>
                </div>
              <? endforeach; ?>
            </div>
            <div>
              <input type="text" name="email" class="form-control" placeholder="Email" required="required">
            </div>
            <div>
              <input type="password" name="pass" class="form-control" placeholder="Password" required="required">
            </div>
            <div>
              <input type="submit" class="btn btn-default submit" value="Log in" />
              <a class="reset_pass" href="/lost_password">Forgot password?</a>
            </div>

            <div class="clearfix"></div>

            <div class="separator">
              <p class="change_link">
                <a href="/signup"> Create Account </a>
              </p>

              <div class="clearfix"></div>
              <br>

              <div>
                <h1 style="font-family: 'Montserrat', sans-serif;">BRUSHWORK'S&nbsp;&nbsp;&nbsp;&nbsp;AFFILIATES</h1>
              </div>
            </div>
          </form>
        </section>
      </div>
    </div>
</div>
