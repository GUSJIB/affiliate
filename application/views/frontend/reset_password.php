<div class="login" style="min-height: 550px;">

    <div class="login_wrapper" style="margin-top: auto;">
      <div class="animate form login_form" style="position: inherit;">
        <section class="login_content">
          <form action="" method="post">
            <h1>Reset Password</h1>
            
            <p class="center">
              <i class="glyphicon glyphicon-ok" style="font-size:75px;color:green;"></i>
              <br/>
              <h4>New password sent to your email already.</h4>
              <br/>
              <a class="btn btn-default" href="/">Click here to Sign In</a>
            </p>

            <div class="clearfix"></div>

          </form>
        </section>
      </div>
    </div>
</div>