<div class="" role="main">
  <div class="">
    
    <div class="clearfix"></div>

    <div class="row">

      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Sign up <small>Blogger</small></h2>
            <div class="clearfix"></div>
          </div>

          <div class="x_content">

            <!-- Smart Wizard -->
            <p>This is a basic form wizard example that inherits the colors from the selected scheme.</p>

            <form action="" class="form-horizontal form-label-left" method="post" novalidate>
            
            <div id="wizard" class="form_wizard wizard_horizontal">
              <ul class="wizard_steps">
                <li>
                  <a href="#step-1">
                    <span class="step_no">1</span>
                    <span class="step_descr">
                      Step 1<br />
                      <small>Details</small>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#step-2">
                    <span class="step_no">2</span>
                    <span class="step_descr">
                      Step 2<br />
                      <small>Contact</small>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#step-3">
                    <span class="step_no">3</span>
                    <span class="step_descr">
                      Step 3<br />
                      <small>Social Network</small>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#step-4">
                    <span class="step_no">4</span>
                    <span class="step_descr">
                      Step 4<br />
                      <small>Payments</small>
                    </span>
                  </a>
                </li>
              </ul>
              <div id="step-1">
                <?php foreach ($this->aauth->get_errors_array() as $error){ ?>
                  <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <?php echo $error; ?>
                  </div>
                <?php } ?>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Email <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="email" class="date-picker form-control col-md-7 col-xs-12" name="email" required="required" type="email">
                  </div>
                </div>                  
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="password" id="password" required="required" name="pass" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="confirm_password">Confirm Password <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="password" id="confirm_password" data-validate-linked="pass" required="required" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">First Name <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="first-name" required="required" name="profile[first_name]" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Middle Name</label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="profile[middle_name]">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Last Name <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="last-name" name="profile[last_name]" required="required" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender</label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div id="gender" class="btn-group" data-toggle="buttons">
                      <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                        <input type="radio" name="profile[gender]" value="male"> &nbsp; Male &nbsp;
                      </label>
                      <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                        <input type="radio" name="profile[gender]" value="female"> Female
                      </label>
                    </div>
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="birthday" class="date-picker form-control col-md-7 col-xs-12" name="profile[dob]" pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])" type="text">
                  </div>
                </div>
              </div>
              <div id="step-2">
                <div class="item form-group">
                  <label for="address" class="control-label col-md-3 col-sm-3 col-xs-12">Address </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="address" class="form-control col-md-7 col-xs-12" type="text" name="profile[address]">
                  </div>
                </div>
                <div class="item form-group">
                  <label for="postal_code" class="control-label col-md-3 col-sm-3 col-xs-12">Postal Code </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="postal_code" class="form-control col-md-7 col-xs-12" type="text" name="profile[postal_code]">
                  </div>
                </div>
                <div class="item form-group">
                  <label for="country" class="control-label col-md-3 col-sm-3 col-xs-12">Country </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="profile[country]" id="country" class="form-control col-md-10" style="float: left;" />
                    <div id="country-container" style="position: relative; float: left; width: 400px; margin: 10px;"></div>
                  </div>
                </div>
                <div class="item form-group">
                  <label for="country" class="control-label col-md-3 col-sm-3 col-xs-12">Country Code </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="country" class="form-control col-md-7 col-xs-12" type="text" name="profile[country_code]">
                  </div>
                </div>
                <div class="item form-group">
                  <label for="contact_number" class="control-label col-md-3 col-sm-3 col-xs-12">Contact Number <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="contact_number" required="required" data-validate-length-range="8,20" class="form-control col-md-7 col-xs-12" type="tel" name="profile[contact_number]">
                  </div>
                </div>
              </div>
              <div id="step-3">
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="blogger_url">Blog URL </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="blogger_url" name="profile[blogger_url]" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="facebook">Facebook </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="facebook" name="profile[facebook]" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="instagram">Instagram </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="instagram" name="profile[instagram]" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="twitter">Twitter </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="twitter" name="profile[twitter]" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="item form-group">
                  <label for="snapchat" class="control-label col-md-3 col-sm-3 col-xs-12">SnapChat </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="snapchat" class="form-control col-md-7 col-xs-12" type="text" name="profile[snapchat]">
                  </div>
                </div>
                <div class="item form-group">
                  <label for="youtube" class="control-label col-md-3 col-sm-3 col-xs-12">Youtube </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="youtube" class="form-control col-md-7 col-xs-12" type="text" name="profile[youtube]">
                  </div>
                </div>
                <div class="item form-group">
                  <label for="other" class="control-label col-md-3 col-sm-3 col-xs-12">Other </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="other" class="form-control col-md-7 col-xs-12" type="text" name="profile[other]">
                  </div>
                </div>
              </div>
              <div id="step-4">
                <div class="item form-group">
                  <label for="recipient_name" class="control-label col-md-3 col-sm-3 col-xs-12">Recipient Name </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="recipient_name" class="form-control col-md-7 col-xs-12" type="text" name="profile[recipient_name]">
                  </div>
                </div>
                <div class="item form-group">
                  <label for="recipient_bank_name" class="control-label col-md-3 col-sm-3 col-xs-12">Bank Recipient Name </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="recipient_bank_name" class="form-control col-md-7 col-xs-12" type="text" name="profile[recipient_bank_name]">
                  </div>
                </div>
                <div class="item form-group">
                  <label for="bank_address" class="control-label col-md-3 col-sm-3 col-xs-12">Bank Address </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="bank_address" class="form-control col-md-7 col-xs-12" type="text" name="profile[bank_address]">
                  </div>
                </div>
                <div class="item form-group">
                  <label for="bank_account_number" class="control-label col-md-3 col-sm-3 col-xs-12">Bank Account Number </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="bank_account_number" class="form-control col-md-7 col-xs-12" type="text" name="profile[bank_account_number]">
                  </div>
                </div>
                <div class="item form-group">
                  <label for="bank_country" class="control-label col-md-3 col-sm-3 col-xs-12">Bank country </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="profile[bank_country]" id="bank_country" class="form-control col-md-10" style="float: left;" />
                    <div id="bank_country-container" style="position: relative; float: left; width: 400px; margin: 10px;"></div>
                  </div>
                </div>
                <div class="item form-group">
                  <label for="bank_routing_code" class="control-label col-md-3 col-sm-3 col-xs-12">Bank Routing Code </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="bank_routing_code" class="form-control col-md-7 col-xs-12" type="text" name="profile[bank_routing_code]">
                  </div>
                </div>
                <div class="item form-group">
                  <label for="paypal_account" class="control-label col-md-3 col-sm-3 col-xs-12">Paypal Account </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="paypal_account" class="form-control col-md-7 col-xs-12" type="text" name="profile[paypal_account]">
                  </div>
                </div>
                <div class="item form-group">
                  <label for="paypal_account" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" id="agree_term" required="required" name="profile[agree_term]" value="1"> I agree <a target="_blank" href="https://www.brushworkcosmetics.com/terms-and-conditions">terms and conditions</a>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            </form>
            <!-- End SmartWizard Content -->

          </div>
        </div>
      </div>
    </div>
  </div>
</div>