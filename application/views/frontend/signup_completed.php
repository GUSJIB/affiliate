<div class="login" style="min-height: 550px;">

    <div class="login_wrapper" style="margin-top: auto;">
      <div class="animate form login_form" style="position: inherit;">
        <section class="login_content">
          <form action="" method="post">
            <h1>Sign Up Completed</h1>
            
            <p class="center">
              <i class="glyphicon glyphicon-ok" style="font-size:75px;color:green;"></i>
              <br/>
              <h3>Please verify your email address.</h3>
              <p class="red">*Please also check your spam and/or junk emails folder</p>
              <br/>
              <a class="btn btn-default" href="/">Click here to Sign In</a>
            </p>

            <div class="clearfix"></div>

          </form>
        </section>
      </div>
    </div>
</div>
