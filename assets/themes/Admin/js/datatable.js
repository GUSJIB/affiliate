$(document).ready(function() {
   
   // Datatable
   $("#datatable-buttons").DataTable({
      dom: "Bfrtip",
      buttons: [
      {
         extend: "copy",
         className: "btn-sm"
      },
      {
         extend: "csv",
         className: "btn-sm"
      },
      {
         extend: "excel",
         className: "btn-sm"
      },
      {
         extend: "pdfHtml5",
         className: "btn-sm"
      },
      {
         extend: "print",
         className: "btn-sm"
      },
      ],
      responsive: true
   });

});