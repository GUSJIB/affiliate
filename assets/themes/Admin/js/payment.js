
$(document).ready(function() {

  $(".select2_single").select2({
    placeholder: "Select a recipient",
    allowClear: true,
    dropdownParent: $('.modal'),
    width: 300
  });

  $('#paid-date').daterangepicker({
    singleDatePicker: true,
    calender_style: "picker_2",
    showDropdowns: true,
    format: "YYYY-MM-DD"
  }, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });

  $.listen('parsley:field:validate', function() {
    validateFront();
  });
  $('#demo-form2 .btn').on('click', function() {
    $('#demo-form2').parsley().validate();
    validateFront();
  });
  var validateFront = function() {
    if (true === $('#demo-form2').parsley().isValid()) {
      $('.bs-callout-info').removeClass('hidden');
      $('.bs-callout-warning').addClass('hidden');
    } else {
      $('.bs-callout-info').addClass('hidden');
      $('.bs-callout-warning').removeClass('hidden');
    }
  }

});