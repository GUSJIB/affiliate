
$(document).ready(function() {

  var minDateFilter;
  var maxDateFilter;
  var table = $('#datatable').DataTable();

  function cb(start, end) {
    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    minDateFilter = start;
    maxDateFilter = end;
    table.draw();
  }

  if($("form#search-order #date-to").val() != "" && $("form#search-order #date-from").val() != ""){
    var from = moment(new Date($("form#search-order #date-from").val()));
    var to = moment(new Date($("form#search-order #date-to").val()));
    cb(from, to);
  }else{
    // cb(moment().subtract(29, 'days'), moment());
    cb(moment(), moment());
  }

  $('#reportrange').daterangepicker({
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
  }, cb);
  
  $.fn.dataTableExt.afnFiltering.push(function( oSettings, aData, iDataIndex ) {
    if ( typeof aData._date == 'undefined' ) {
      aData._date = new Date(aData[1]).getTime();
    }

    if ( minDateFilter && !isNaN(minDateFilter) ) {
      if ( aData._date < minDateFilter ) {
        return false;
      }
    }
    if ( maxDateFilter && !isNaN(maxDateFilter) ) {
      if ( aData._date > maxDateFilter ) {
        return false;
      }
    }

    return true;
  });

});